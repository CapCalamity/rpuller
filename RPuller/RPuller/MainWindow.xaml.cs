﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RedditSharp;
using RedditSharp.Things;
using RPuller;

namespace RPuller
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Controller Controller { get; set; }
        private KeyValuePair<Subreddit, Category> CurrentEdit { get; set; }
        public Settings Settings { get; set; }
        public BindingList<DownloadProgress> DownloadStatus { get; set; }
        public int TokenLimit { get; set; }
        public int TokensRemaining { get; set; }
        public int UserLimit { get; set; }
        public int UserRemaining { get; set; }
        public long UserReset { get; set; }

        internal MainWindow(Controller con, Settings set)
        {
            Settings = set;
            Controller = con;
            DownloadStatus = new BindingList<DownloadProgress>();
            InitializeComponent();
        }

        /// <summary>
        /// Convert string to SolidColorBrush
        /// </summary>
        /// <param name="code">string defining Hex/RGBA encoded color</param>
        /// <returns>Brush</returns>
        private Brush ToBrush(string code)
        {
            try
            {
                return (SolidColorBrush) (new BrushConverter().ConvertFrom(code));
            }
            catch (Exception)
            {
                return Brushes.Black;
            }
        }

        private void StartFetchButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.StartFetch();
        }

        private void StopFetchButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.StopFetch();
        }

        public void SetFetchButtonEnabled(bool enabled)
        {
            StartFetchButton.IsEnabled = enabled;
            StopFetchButton.IsEnabled = !enabled;
            AddSubreddit.IsEnabled = enabled;
            RemoveSubReddit.IsEnabled = enabled;
            AmountOverallBox.IsEnabled = enabled;
            ConcurrentDownloads.IsEnabled = enabled;
            AllowNSFW.IsEnabled = enabled;
            SubredditList.IsEnabled = enabled;
            StorageLocationBox.IsEnabled = enabled;
            EnableHistory.IsEnabled = enabled;
            FilenameTemplateBox.IsEnabled = enabled;
        }

        private void AddSubreddit_Click(object sender, RoutedEventArgs e)
        {
            string name = SubredditName.Text;

            if (String.IsNullOrWhiteSpace(name))
                return;

            Category category;
            ComboBoxItem selectedItem = (ComboBoxItem) Category.SelectedItem;
            category = TagToCategory((string) selectedItem.Tag);

            SetEnabledAddSubredditButtons(false);
            Controller.AddSubreddit(name, category);
            SetEnabledAddSubredditButtons(true);
        }

        private void SetEnabledAddSubredditButtons(bool enabled)
        {
            SubredditName.IsEnabled = enabled;
            Category.IsEnabled = enabled;
            AddSubreddit.IsEnabled = enabled;
            RemoveSubReddit.IsEnabled = enabled;
            FavoritesList.IsEnabled = enabled;
            StartFetchButton.IsEnabled = enabled;
            StopFetchButton.IsEnabled = !enabled;
        }

        internal bool IsInSubredditList(string name, Category category)
        {
            foreach (ListItem item in SubredditList.Items)
            {
                if (item.Name == name && item.Category == category)
                    return true;
            }
            return false;
        }

        internal void DisplaySubreddits(List<KeyValuePair<Subreddit, RPuller.Category>> subreddits)
        {
            SubredditList.Items.Clear();

            foreach (KeyValuePair<Subreddit, RPuller.Category> kvp in subreddits)
            {
                SubredditList.Items.Add(new ListItem(kvp.Key, kvp.Value));
            }
        }

        private void RemoveSubReddit_Click(object sender, RoutedEventArgs e)
        {
            if (SubredditList.SelectedItem == null)
                return;

            Controller.RemoveSubreddits(GetCurrentSubredditSelection());
        }

        internal List<KeyValuePair<Subreddit, Category>> GetCurrentSubredditSelection()
        {
            if (SubredditList.SelectedItem == null)
                return new List<KeyValuePair<Subreddit, Category>>();

            var items = new List<KeyValuePair<Subreddit, Category>>(SubredditList.SelectedItems.Count);
            foreach (var i in SubredditList.SelectedItems)
            {
                var item = (ListItem) i;
                items.Add(new KeyValuePair<Subreddit, Category>(item.Subreddit, item.Category));
            }

            return items;
        }

        internal List<KeyValuePair<string, Category>> GetCurrentFavoriteSelection()
        {
            if (FavoritesList.SelectedItem == null)
                return new List<KeyValuePair<string, Category>>();

            var items = new List<KeyValuePair<string, Category>>(FavoritesList.SelectedItems.Count);
            foreach (var i in FavoritesList.SelectedItems)
            {
                items.Add((KeyValuePair<string, Category>) i);
            }

            return items;
        }

        internal void DisplayDownloads(IEnumerable<DownloadProgress> downloads)
        {
            ProgressList.Items.Clear();

            foreach (DownloadProgress down in downloads)
            {
                int id = ProgressList.Items.Add(down);
            }
        }

        internal void DisplayFavorites(IEnumerable<KeyValuePair<string, Category>> favorites)
        {
            FavoritesList.Items.Clear();

            foreach (var kvp in favorites)
                FavoritesList.Items.Add(new ListItem(kvp.Key, kvp.Value));
        }

        private Category TagToCategory(string tag)
        {
            switch (tag)
            {
                default:
                    return RPuller.Category.Hot;
                case "hot":
                    return RPuller.Category.Hot;
                case "new":
                    return RPuller.Category.New;
                case "top_hour":
                    return RPuller.Category.TopHour;
                case "top_day":
                    return RPuller.Category.TopDay;
                case "top_week":
                    return RPuller.Category.TopWeek;
                case "top_month":
                    return RPuller.Category.TopMonth;
                case "top_year":
                    return RPuller.Category.TopYear;
                case "top_all":
                    return RPuller.Category.TopAll;
            }
        }

        private string CategoryToTag(Category cat)
        {
            switch (cat)
            {
                default:
                case RPuller.Category.Hot:
                    return "hot";
                case RPuller.Category.New:
                    return "new";
                case RPuller.Category.TopHour:
                    return "top_hour";
                case RPuller.Category.TopDay:
                    return "top_day";
                case RPuller.Category.TopWeek:
                    return "top_week";
                case RPuller.Category.TopMonth:
                    return "top_month";
                case RPuller.Category.TopYear:
                    return "top_year";
                case RPuller.Category.TopAll:
                    return "top_all";
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !Controller.WindowClosing();
        }

        private void AddToFavoriteButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.AddFavorites(GetCurrentSubredditSelection());
        }

        private void AddFromFavoriteButton_Click(object sender, RoutedEventArgs e)
        {
            SetEnabledAddSubredditButtons(false);
            Controller.AddFromFavorites(GetCurrentFavoriteSelection());
            SetEnabledAddSubredditButtons(true);
        }

        private void FavoriteSubreddit_Click(object sender, RoutedEventArgs e)
        {
            Controller.AddFavorites(GetCurrentSubredditSelection());
        }

        private void UnfavoriteSubreddit_Click(object sender, RoutedEventArgs e)
        {
            Controller.RemoveFavorites(GetCurrentFavoriteSelection());
        }

        private void FavoritesList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SetEnabledAddSubredditButtons(false);
            Controller.AddFromFavorites(GetCurrentFavoriteSelection());
            SetEnabledAddSubredditButtons(true);
        }

        private class ListItem
        {
            public Subreddit Subreddit { get; private set; }
            public string Name { get; private set; }
            public Category Category { get; private set; }

            public ListItem(string name, Category category)
            {
                Subreddit = default(Subreddit);
                Name = name;
                Category = category;
            }

            public ListItem(Subreddit sub, Category category)
            {
                Subreddit = sub;
                Category = category;
                Name = sub.Name;
            }
        }

        private void DescribableOption_MouseEnter(object sender, MouseEventArgs e)
        {

        }
    }
}
