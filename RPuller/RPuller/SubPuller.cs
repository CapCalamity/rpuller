﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedditSharp;
using RedditSharp.Things;

namespace RPuller
{
    class SubPuller
    {
        private const int DEFAULT_PAGE_SIZE = 5;

        public Subreddit Subreddit { get; private set; }
        public Category Category { get; private set; }
        private Listing<Post> Listing { get; set; }
        public int CurrentPostNr { get; private set; }
        public int PageSize { get; set; }
        public event OnPostResolvedHandler OnPostResolved;
        public delegate void OnPostResolvedHandler(Post p);

        public SubPuller(Subreddit sub, Category cat)
        {
            Subreddit = sub;
            Category = cat;
            Listing = GetListing(Subreddit, Category);
            CurrentPostNr = 0;
            PageSize = DEFAULT_PAGE_SIZE;
        }

        private Listing<Post> GetListing(Subreddit sub, Category cat)
        {
            Listing<Post> listing;

            switch (cat)
            {
                default:
                case Category.Hot:
                    listing = sub.Hot;
                    break;
                case Category.New:
                    listing = sub.New;
                    break;
                case Category.TopHour:
                    listing = sub.GetTop(FromTime.Hour);
                    break;
                case Category.TopDay:
                    listing = sub.GetTop(FromTime.Day);
                    break;
                case Category.TopWeek:
                    listing = sub.GetTop(FromTime.Week);
                    break;
                case Category.TopMonth:
                    listing = sub.GetTop(FromTime.Month);
                    break;
                case Category.TopYear:
                    listing = sub.GetTop(FromTime.Year);
                    break;
                case Category.TopAll:
                    listing = sub.GetTop(FromTime.All);
                    break;
            }

            return listing;
        }

        public void PullAll()
        {
            foreach (var post in Listing.Skip(CurrentPostNr))
            {
                CurrentPostNr++;
                if (OnPostResolved != null)
                    OnPostResolved(post);
            }
        }

        public void Pull()
        {
            foreach (var post in Listing.Skip(CurrentPostNr).Take(PageSize))
                if (OnPostResolved != null)
                    OnPostResolved(post);
            CurrentPostNr += PageSize;
        }
    }
}
