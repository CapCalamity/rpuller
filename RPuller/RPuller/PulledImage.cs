﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using System.Timers;
using RedditSharp.Things;

namespace RPuller
{
    class PulledImage
    {
        private static volatile ConcurrentDictionary<string, int> Counters = new ConcurrentDictionary<string, int>();
        private static readonly Regex PathRegex = new Regex(
            String.Format("[{0}]", Regex.Escape(new String(System.IO.Path.GetInvalidPathChars()) + ":<>?|*")));
        private static readonly Regex FileRegex = new Regex(
            String.Format("[{0}]", Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()))));

        public string Link { get; private set; }
        public string Name { get; private set; }
        public string FullName { get { return (DirectoryInfo != null ? DirectoryInfo.FullName + Name : Path + Name); } }
        public string Path { get; private set; }
        public string Title { get; private set; }
        public bool Album { get; private set; }
        public DownloadStatus Status { get; private set; }
        public DirectoryInfo DirectoryInfo { get; private set; }
        public DateTime LastUpdate { get; private set; }
        public Post SourcePost { get; private set; }

        public PulledImage(Post post, string link, bool album, Settings settings)
        {
            Link = link;
            Title = post.Title;
            Album = album;
            SourcePost = post;

            if (Album)
            {
                if (!Counters.ContainsKey(Title))
                    Counters[Title] = 0;
                Counters[Title]++;
            }

            //create the filename by replacing all occurences of the available placeholders with its value, then removing all illegal characters
            Name = FileRegex.Replace(
                settings.FileTemplate
                .Replace("{sub}", SourcePost.Subreddit)
                .Replace("{name}", Album ? Counters[Title].ToString() : Title)
                .Replace("{date}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
                .Replace("{timestamp}", GetTimestamp().ToString())
                .Replace("{type}", GetFileType())
                .Replace("{album}", Album ? Title : "")
                .Replace("{link}", Link)
                .Replace("{author}", post.AuthorName), "");

            Path = PathRegex.Replace(settings.StorageLocation + settings.PathTemplate
                .Replace("{sub}", SourcePost.Subreddit)
                .Replace("{name}", Album ? Counters[Title].ToString() : Title.Replace("\\", ""))
                .Replace("{date}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
                .Replace("{timestamp}", GetTimestamp().ToString())
                .Replace("{type}", GetFileType())
                .Replace("{album}", Album ? Title : "")
                .Replace("{link}", Link)
                .Replace("{author}", SourcePost.AuthorName), "");

            Status = DownloadStatus.NotStarted;

            DirectoryInfo = new DirectoryInfo(Path);
        }

        private int GetTimestamp()
        {
            return (Int32) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public async void DownloadAsync(DownloadProgressChangedEventHandler progressChangedHandler = null,
                             System.ComponentModel.AsyncCompletedEventHandler downloadCompletedHandler = null,
                             int timeoutSec = 60)
        {
            var t = Task.Run(() =>
            {
                var client = new WebClientWithTimeout(timeoutSec * 1000);
                string realPath = System.IO.Path.GetDirectoryName(Path + Name);
                string fullname = System.IO.Path.GetFullPath(Path + Name);

                try
                {
                    if (!Directory.Exists(realPath))
                        Directory.CreateDirectory(realPath);

                    if (progressChangedHandler != null)
                        client.DownloadProgressChanged += progressChangedHandler;

                    if (downloadCompletedHandler != null)
                        client.DownloadFileCompleted += downloadCompletedHandler;

                    client.DownloadFileCompleted += client_DownloadFileCompleted;
                    client.DownloadProgressChanged += client_DownloadProgressChanged;

                    Status = DownloadStatus.Running;

                    client.DownloadFileAsync(new Uri(Link), fullname);
                }
                catch (Exception)
                {
                    Status = DownloadStatus.Failure;
                    client.CancelAsync();
                    if (File.Exists(fullname))
                        File.Delete(fullname);
                }
            });
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            LastUpdate = DateTime.Now;
        }

        void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
                Status = DownloadStatus.Failure;
            else
                Status = DownloadStatus.Success;
        }

        public string GetFileType()
        {
            string extension = System.IO.Path.GetExtension(Link).Substring(1);

            int index = extension.IndexOf("?");
            if (index != -1)
            {
                extension = extension.Substring(0, index);
            }

            return extension;
        }
    }

    enum DownloadStatus
    {
        NotStarted,
        Running,
        Success,
        Failure
    }
}
