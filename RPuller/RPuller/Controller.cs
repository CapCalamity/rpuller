﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Windows.Media;
using System.ComponentModel;
using RedditSharp;
using RedditSharp.Things;
using System.Collections.Concurrent;

namespace RPuller
{
    class Controller
    {
        private static readonly int POSTS_PER_SUB = 5;
        private readonly string[] imgurDomains = new string[] { "i.imgur.com", "imgur.com" };
        private readonly string[] deeplinkFileTypes = new string[] { ".jpg", ".jpeg", ".gif", ".bmp", ".png" };

        private static MainWindow MainWindow { get; set; }

        private ConcurrentQueue<PulledImage> DownloadQueue { get; set; }
        //@TODO: change to dictionary
        private List<KeyValuePair<Subreddit, Category>> Subreddits { get; set; }
        private int NumCurrentDownloads { get; set; }
        private Reddit Reddit { get; set; }
        private Thread FetchThread { get; set; }
        private bool RunFetchThread { get; set; }
        private Thread UIThread { get; set; }
        private bool RunUIThread { get; set; }
        private Thread DisplayThread { get; set; }
        private bool RunDisplayThread { get; set; }
        private Thread DownloadThread { get; set; }
        private Timer DLSPeedTimer { get; set; }
        private long DownloadedThisSecond { get; set; }
        private int ReturnCode { get; set; }
        private bool Fetching { get; set; }
        private bool Running { get; set; }

        internal Settings Settings { get; set; }

        public Controller()
        {
            NumCurrentDownloads = 0;
            Settings = Settings.LoadFromFile();
            Fetching = false;
            DownloadQueue = new ConcurrentQueue<PulledImage>();
            Reddit = new RedditSharp.Reddit();
            Subreddits = new List<KeyValuePair<Subreddit, Category>>();
            DownloadedThisSecond = 0;
            DLSPeedTimer = new Timer(new TimerCallback(CalculateCurrentSpeed), null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
            UIThread = new Thread(new ThreadStart(RunUI));
            UIThread.SetApartmentState(ApartmentState.STA);
            ReturnCode = 0;
            Running = true;
        }

        private bool Initialize()
        {
            try
            {
                ImageHistory.Initialize();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int Run()
        {
            if (Initialize())
            {
                RunUIThread = true;
                UIThread.Start();

                while (Running && UIThread.IsAlive)
                {
                    Thread.Yield();
                    Thread.Sleep(100);
                }
            }
            else
                MessageBox.Show("Could not initialize", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            return ReturnCode;
        }

        #region Thread Start/Stop Methods
        public void StartFetch()
        {
            if (Fetching)
                return;

            DownloadThread = new Thread(new ThreadStart(SaveImages));
            DownloadThread.Start();

            FetchThread = new Thread(new ParameterizedThreadStart(FetchFromSubreddits));
            FetchThread.SetApartmentState(ApartmentState.STA);
            Fetching = true;
            FetchThread.Start(Subreddits);

            MainWindow.Dispatcher.InvokeAsync(() => MainWindow.SetFetchButtonEnabled(false));
        }

        internal void StopFetch()
        {
            RunFetchThread = false;
            Fetching = false;
            MainWindow.Dispatcher.InvokeAsync(() => MainWindow.SetFetchButtonEnabled(true));

            try { FetchThread = null; }
            catch (Exception) { }
            try { DownloadThread = null; }
            catch (Exception) { }
        }
        #endregion

        #region Thread Methods
        private void RunUI()
        {
            try
            {
                MainWindow = new MainWindow(this, Settings);
                MainWindow.DataContext = MainWindow;
                MainWindow.ShowDialog();
                Running = false;
            }
            catch (ThreadAbortException)
            { return; }
        }

        private void FetchFromSubreddits(object subredditsObj)
        {
            if (subredditsObj == null || !(subredditsObj is List<KeyValuePair<Subreddit, Category>>))
            {
                StopFetch();
                return;
            }

            var subreddits = subredditsObj as List<KeyValuePair<Subreddit, Category>>;

            if (subreddits.Count < 1)
            {
                StopFetch();
                return;
            }

            var reddit = new Reddit();
            var pullers = new Dictionary<SubPuller, Thread>();

            try
            {

                foreach (var kvp in subreddits)
                {
                    var puller = new SubPuller(kvp.Key, kvp.Value);
                    var thread = new Thread(new ThreadStart(() =>
                    {
                        try
                        {
                            puller.OnPostResolved += (post) =>
                            {
                                if (post.NSFW && !Settings.AllowNSFW)
                                    return;

                                if (!LinkAlreadyDownloaded(post.Url.AbsoluteUri))
                                    ResolvePost(post);
                            };

                            while (Fetching)
                                puller.Pull();
                        }
                        catch (Exception)
                        {
                            return;
                        }
                    }));

                    pullers.Add(puller, thread);
                }

                foreach (var kvp in pullers)
                    kvp.Value.Start();

                bool allThreadsFinished;
                do
                {
                    allThreadsFinished = true;
                    foreach (var kvp in pullers)
                        if (!kvp.Value.IsAlive)
                            allThreadsFinished = false;
                }
                while (!allThreadsFinished);
            }
            catch (Exception)
            {
                foreach (var kvp in pullers)
                    if (kvp.Value != null && kvp.Value.IsAlive)
                        kvp.Value.Abort();
            }
        }

        private void ResolvePost(Post post, Action callback = null)
        {
            new Thread(new ThreadStart(() =>
            {
                try
                {
                    if (imgurDomains.Any(d => post.Domain.ToLowerInvariant().EndsWith(d)))
                    {
                        if (!Fetching) return;

                        SetCurrentStatus(post.Url.AbsolutePath, "Dereferencing Link");

                        var imgurResolver = new ImgurResolver(post.Url);
                        Settings.ImgurApplicationTokenLimit = ImgurResolver.TokenLimit;
                        Settings.ImgurApplicationTokensRemaining = ImgurResolver.TokensRemaining;
                        Settings.ImgurUserTokenLimit = ImgurResolver.UserLimit;
                        Settings.ImgurUserTokensRemaining = ImgurResolver.UserRemaining;
                        Settings.ImgurUserReset = ImgurResolver.UserReset;

                        IEnumerable<PulledImage> pulled;
                        try
                        {
                            pulled = ResolveImages(imgurResolver, post);
                        }
                        catch (Exception) { return; }

                        foreach (var item in pulled)
                            DownloadQueue.Enqueue(item);
                    }
                    else if (deeplinkFileTypes.Any(dft => post.Url.AbsoluteUri.ToLowerInvariant().EndsWith(dft)))
                    {
                        SetCurrentStatus(post.Url.AbsoluteUri, "Adding to queue");
                        DownloadQueue.Enqueue(new PulledImage(post, post.Url.AbsoluteUri, false, Settings));
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    if (callback != null)
                        callback();
                }
            })).Start();
        }

        private void CalculateCurrentSpeed(object state)
        {
            Debug.WriteLine("QueueLenght: " + DownloadQueue.Count);

            if (MainWindow == null)
                return;

            string template = "{0:0.##} {1}/s";
            string unit = "B";
            double speed = 0.0D;

            if (DownloadedThisSecond < 1024)
            {
                speed = DownloadedThisSecond;
                unit = "B";
            }
            else if (DownloadedThisSecond < (1024 * 1024))
            {
                speed = DownloadedThisSecond / 1024.0D;
                unit = "kB";
            }
            else
            {
                speed = DownloadedThisSecond / 1024.0D / 1024.0D;
                unit = "mB";
            }

            MainWindow.Dispatcher.InvokeAsync(() =>
            {
                MainWindow.CurrentSpeed.Text = String.Format(template, speed, unit);
            });

            DownloadedThisSecond = 0;

            return;
        }

        private void SaveImages()
        {
            do
            {
                PulledImage pulled = null;

                if (DownloadQueue.Count <= 0 || (!DownloadQueue.TryDequeue(out pulled) && pulled == null))
                {
                    Thread.Sleep(1000);
                    continue;
                }
                else
                {
                    NumCurrentDownloads++;
                    DownloadProgress dProgress = new DownloadProgress(pulled.Link, pulled.Name, pulled.Album ? pulled.DirectoryInfo.Name : String.Empty);
                    MainWindow.Dispatcher.InvokeAsync(() => MainWindow.DownloadStatus.Add(dProgress));
                    pulled.DownloadAsync(
                        (sender, eArgs) =>
                        {
                            try
                            {
                                dProgress.Total = eArgs.TotalBytesToReceive;
                                dProgress.Received = eArgs.BytesReceived;
                                DownloadedThisSecond += dProgress.Difference;
                            }
                            catch (Exception) { }
                        },
                        (sender, eArgs) =>
                        {
                            NumCurrentDownloads--;
                            MainWindow.Dispatcher.InvokeAsync(() => MainWindow.DownloadStatus.Remove(dProgress));

                            if (Settings.EnableHistory && !ImageHistory.Contains(pulled.SourcePost.Url.AbsoluteUri))
                                ImageHistory.Add(pulled.SourcePost.Url.AbsoluteUri, pulled.Title);
                        });

                    while (NumCurrentDownloads >= (Settings.MaxConcurrentDownloads * 0.6))
                    {
                        if (!Fetching)
                            return;
                        Thread.Sleep(100);
                    }
                }
            }
            while (Fetching);
        }
        #endregion

        private bool LinkAlreadyDownloaded(string link)
        {
            return (Settings.EnableHistory && ImageHistory.Contains(link));
        }

        private IEnumerable<PulledImage> ResolveImages(IResolver resolver, Post post)
        {
            var queue = new List<PulledImage>();

            string[] resolvedUris = resolver.ResolveURI().ToArray();
            bool album = resolvedUris.Length > 1;
            string title = post.Permalink.OriginalString.TrimEnd('/').Split('/').Last();

            foreach (string res in resolvedUris)
            {
                if (!Fetching)
                    return queue;

                if (String.IsNullOrWhiteSpace(res))
                    continue;

                if (LinkAlreadyDownloaded(res))
                {
                    SetCurrentStatus(post.Url.AbsoluteUri, "Link already downloaded");
                    continue;
                }
                else
                {
                    SetCurrentStatus(post.Url.AbsoluteUri, "Adding to queue");
                    queue.Add(new PulledImage(post, res, album, Settings));
                }
            }

            return queue;
        }

        /// <summary>
        /// Respond to the WindowClosing signal.<para/>
        /// </summary>
        /// <returns>
        /// Returns true to signal the window may be closed.<para/>
        /// Returns false to stop the window from Closing
        /// </returns>
        internal bool WindowClosing()
        {
            if (!Fetching)
            {
                if (DLSPeedTimer != null)
                    DLSPeedTimer.Change(0, Timeout.Infinite);
                if (FetchThread != null && FetchThread.IsAlive)
                    RunFetchThread = false;

                Settings.SaveToFile();

                return true;
            }
            else
            {
                MessageBox.Show("Please stop all Downloads before closing the window.");
                return false;
            }
        }

        void DirectDisplay_PostImageShown(string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch { }
        }

        private void SetCurrentStatus(string status, string statusGroup)
        {
            MainWindow.Dispatcher.InvokeAsync(() =>
            {
                MainWindow.CurrentAction.Text = statusGroup;
                MainWindow.CurrentFile.Text = status;
            });
        }

        public void AddSubreddit(string name, Category category)
        {
            if (!MainWindow.IsInSubredditList(name, category))
            {
                Subreddit sub = Reddit.GetSubreddit(name);
                if (sub != null)
                {
                    Subreddits.Add(new KeyValuePair<Subreddit, Category>(sub, category));
                    SubredditsChanged();
                }
            }
        }

        public void RemoveSubreddits(List<KeyValuePair<Subreddit, Category>> subreddits)
        {
            foreach (var subreddit in Subreddits)
                Subreddits.Remove(subreddit);
            SubredditsChanged();
        }

        private void SubredditsChanged()
        {
            MainWindow.Dispatcher.InvokeAsync(() =>
            {
                MainWindow.DisplaySubreddits(Subreddits);
            });
        }

        private Listing<Post> GetListing(Subreddit sub, Category cat)
        {
            Listing<Post> listing;

            switch (cat)
            {
                default:
                case Category.Hot:
                    listing = sub.Hot;
                    break;
                case Category.New:
                    listing = sub.New;
                    break;
                case Category.TopHour:
                    listing = sub.GetTop(FromTime.Hour);
                    break;
                case Category.TopDay:
                    listing = sub.GetTop(FromTime.Day);
                    break;
                case Category.TopWeek:
                    listing = sub.GetTop(FromTime.Week);
                    break;
                case Category.TopMonth:
                    listing = sub.GetTop(FromTime.Month);
                    break;
                case Category.TopYear:
                    listing = sub.GetTop(FromTime.Year);
                    break;
                case Category.TopAll:
                    listing = sub.GetTop(FromTime.All);
                    break;
            }

            return listing;
        }

        public void AddFavorites(List<KeyValuePair<Subreddit, Category>> favorites)
        {
            foreach (var favorite in favorites)
            {
                var fav = new KeyValuePair<string, Category>(favorite.Key.DisplayName, favorite.Value);
                if (!Settings.Favorites.Contains(fav))
                    MainWindow.Dispatcher.Invoke(() => Settings.Favorites.Add(fav));
            }
        }

        public void RemoveFavorites(List<KeyValuePair<string, Category>> favorites)
        {
            foreach (var favorite in favorites)
            {
                var fav = new KeyValuePair<string, Category>(favorite.Key, favorite.Value);
                if (Settings.Favorites.Contains(fav))
                    MainWindow.Dispatcher.Invoke(() => Settings.Favorites.Remove(fav));
            }
        }

        public void AddFromFavorites(List<KeyValuePair<string, Category>> favorites)
        {
            if (Fetching)
                return;

            foreach (var fav in favorites)
                AddSubreddit(fav.Key, fav.Value);
        }
    }

    public enum Category
    {
        Hot,
        New,
        TopHour,
        TopDay,
        TopWeek,
        TopMonth,
        TopYear,
        TopAll
    }
}
