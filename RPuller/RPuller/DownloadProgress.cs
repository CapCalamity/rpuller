﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RPuller
{
    public class DownloadProgress : INotifyPropertyChanged
    {
        #region INotifiyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion

        private long _Total;
        public long Total
        {
            get { return _Total; }
            set { SetField(ref _Total, value); }
        }

        private long _Received;
        public long Received
        {
            get { return _Received; }
            set
            {
                Difference = value - Received;
                SetField(ref _Received, value);
                OnPropertyChanged("Percentage");
            }
        }

        public double Percentage
        {
            get { return (Received <= 1 || Total <= 0) ? 0.0 : 100.0 / Total * Received; }
        }

        public string Link { get; private set; }
        public long Difference { get; private set; }
        public string Name { get; private set; }
        public string Album { get; private set; }

        public DownloadProgress(string link, string name, string album)
        {
            Link = link;
            Total = 0;
            Received = 0;
            Name = name;
            Album = album;
        }
    }
}
