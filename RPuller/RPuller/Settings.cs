﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using RedditSharp;
using RedditSharp.Things;

namespace RPuller
{
    public class Settings : INotifyPropertyChanged
    {
        private static readonly string SAVE_FILE = "settings.dat";

        // boiler-plate
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        /// <summary>
        /// Limit after which the Downloader will stop
        /// </summary>
        private int _OverallAmount;
        public int OverallAmount
        {
            get { return _OverallAmount; }
            set { SetField(ref _OverallAmount, value); }
        }

        /// <summary>
        /// Whether to skip NSFW images or not
        /// </summary>
        private bool _AllowNSFW;
        public bool AllowNSFW
        {
            get { return _AllowNSFW; }
            set { SetField(ref _AllowNSFW, value); }
        }

        /// <summary>
        /// Number of Concurrent downloads
        /// </summary>
        public int MaxConcurrentDownloads
        {
            get { return ServicePointManager.DefaultConnectionLimit; }
            set { ServicePointManager.DefaultConnectionLimit = (value > 0 ? value : 1); }
        }

        /// <summary>
        /// Bookkeeping of current DLSpeed
        /// </summary>
        private string _CurrentDLSpeed;
        public string CurrentDLSpeed
        {
            get { return _CurrentDLSpeed; }
            set { SetField(ref _CurrentDLSpeed, value); }
        }

        /// <summary>
        /// Enable History of items, to not download things twice
        /// </summary>
        private bool _EnableHistory;
        public bool EnableHistory
        {
            get { return _EnableHistory; }
            set { SetField(ref _EnableHistory, value); }
        }

        /// <summary>
        /// Where to store images
        /// </summary>
        private string _StorageLocation;
        public string StorageLocation
        {
            get { return _StorageLocation; }
            set { SetField(ref _StorageLocation, value); }
        }

        /// <summary>
        /// How to name the downloaded images
        /// </summary>
        private string _FileTemplate;
        public string FileTemplate
        {
            get { return _FileTemplate; }
            set { SetField(ref _FileTemplate, value); }
        }

        /// <summary>
        /// How to name the downloaded images
        /// </summary>
        private string _PathTemplate;
        public string PathTemplate
        {
            get { return _PathTemplate; }
            set { SetField(ref _PathTemplate, value); }
        }

        /// <summary>
        /// List of favorite subreddits<para/>
        /// Stored as list of names
        /// </summary>
        private BindingList<KeyValuePair<string, Category>> _Favorites;
        public BindingList<KeyValuePair<string, Category>> Favorites
        {
            get { return _Favorites; }
            set { SetField(ref _Favorites, value); }
        }

        private int _ImgurApplicationTokenLimit;
        public int ImgurApplicationTokenLimit
        {
            get { return _ImgurApplicationTokenLimit; }
            set { SetField(ref _ImgurApplicationTokenLimit, value); }
        }

        private int _ImgurApplicationTokensRemaining;
        public int ImgurApplicationTokensRemaining
        {
            get { return _ImgurApplicationTokensRemaining; }
            set { SetField(ref _ImgurApplicationTokensRemaining, value); }
        }

        private int _ImgurUserTokenLimit;
        public int ImgurUserTokenLimit
        {
            get { return _ImgurUserTokenLimit; }
            set { SetField(ref _ImgurUserTokenLimit, value); }
        }

        private int _ImgurUserTokensRemaining;
        public int ImgurUserTokensRemaining
        {
            get { return _ImgurUserTokensRemaining; }
            set { SetField(ref _ImgurUserTokensRemaining, value); }
        }

        private long _ImgurUserReset;
        public long ImgurUserReset
        {
            get { return _ImgurUserReset; }
            set { SetField(ref _ImgurUserReset, value); }
        }

        public Settings()
        {
            ImgurApplicationTokenLimit = 0;
            ImgurApplicationTokensRemaining = 0;
            ImgurUserTokenLimit = 0;
            ImgurUserTokensRemaining = 0;
            ImgurUserReset = 0;
            MaxConcurrentDownloads = 2;
            OverallAmount = 20;
            AllowNSFW = false;
            CurrentDLSpeed = "";
            EnableHistory = true;
            StorageLocation = "./";
            FileTemplate = "";
            PathTemplate = "";
            Favorites = new BindingList<KeyValuePair<string, Category>>();
        }

        public bool SaveToFile()
        {
            try
            {
                //locks up when _Favorites is filled with at least one object
                string serialized = JsonConvert.SerializeObject(this, Formatting.Indented);

                File.WriteAllText(SAVE_FILE, serialized, Encoding.UTF8);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Settings LoadFromFile()
        {
            try
            {
                string json = File.ReadAllText(SAVE_FILE, Encoding.UTF8);

                return JsonConvert.DeserializeObject<Settings>(json);
            }
            catch (Exception)
            {
                return new Settings();
            }
        }
    }
}
