﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace RPuller
{
    class ImgurResolver : IResolver
    {
        private static string ApiURL = "https://api.imgur.com/3/";
        public static int TokenLimit { get; private set; }
        public static int TokensRemaining { get; private set; }
        public static int UserLimit { get; private set; }
        public static int UserRemaining { get; private set; }
        public static long UserReset { get; private set; }

        private Uri BaseURI { get; set; }

        public ImgurResolver(Uri uri)
        {
            BaseURI = uri;
        }

        private string DownloadFrom(Uri url)
        {
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("Authorization", "Client-ID " + Properties.Resources.ImgurClientID);
                string raw = client.DownloadString(url);
                TokensRemaining = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-ClientRemaining"]);
                TokenLimit = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-ClientLimit"]);
                UserLimit = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-UserLimit"]);
                UserRemaining = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-UserRemaining"]);
                UserRemaining = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-UserRemaining"]);
                UserReset = Convert.ToInt64(client.ResponseHeaders["X-RateLimit-UserReset"]);

                return raw;
            }
            catch (WebException) { return ""; }
        }

        private IEnumerable<string> ResolveAlbum()
        {
            List<string> items = new List<string>();

            try
            {
                string albumID = BaseURI.AbsolutePath.StartsWith("/a/") ? BaseURI.AbsolutePath.Substring("/a/".Length) : String.Empty;
                if (albumID == String.Empty)
                    return new String[] { };

                string api = ApiURL + "album/" + albumID;
                if (api.EndsWith("/"))
                    api = api.TrimEnd('/');

                string raw = DownloadFrom(new Uri(api));

                if (raw == "")
                    return items;

                dynamic json = JsonConvert.DeserializeObject(raw);

                foreach (dynamic img in json["data"]["images"])
                    items.Add(img["link"].Value);
            }
            catch (Exception e)
            {
                items.Clear();
                string s = "could not download from " + BaseURI + ", " + e.Message;
            }

            return items;
        }

        private string ResolveImage()
        {
            string retVal = String.Empty;
            try
            {
                string api = ApiURL + "image/" + BaseURI.AbsolutePath.TrimEnd('/').Split('/').Last();
                if (api.EndsWith("/"))
                    api = api.TrimEnd('/');

                string raw = DownloadFrom(new Uri(api));

                if (raw == "")
                    return retVal;

                dynamic json = JsonConvert.DeserializeObject(raw);

                retVal = json["data"]["link"].Value;
            }
            catch (Exception e)
            {
                retVal = String.Empty;
                string s = "could not download from " + BaseURI + ", " + e.Message;
            }

            return retVal;
        }

        public IEnumerable<string> ResolveURI()
        {
            if (new string[] { ".jpg", ".jpeg", ".gif", ".bmp", ".png" }.Any(t => BaseURI.AbsolutePath.EndsWith(t)))
            {
                return new string[] { BaseURI.AbsoluteUri };
            }

            //if it is an album
            if (BaseURI.AbsoluteUri.Contains("/a/"))
            {
                return ResolveAlbum();
            }
            else
            {
                return new string[] { ResolveImage() };
            }
        }
    }
}
