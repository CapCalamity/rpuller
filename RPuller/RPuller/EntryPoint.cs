﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPuller
{
    class EntryPoint
    {
        [STAThread]
        public static int Main(string[] args)
        {
            Controller controller = new Controller();
            return controller.Run();
        }
    }
}
