﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace RPuller
{
    static class ImageHistory
    {
        private static SQLiteConnection DatabaseConnection = null;
        private static string ConnectionString = "Data Source={0}; Version=3;";
        private static readonly string TABLE_NAME = "DownloadedLinks";
        private static readonly string TABLE_DEFINITION = "(History_id INTEGER PRIMARY KEY, Link TEXT, Title TEXT, DownloadedOn TEXT)";

        public static void Initialize()
        {
            OpenDB();
        }

        private static void OpenDB()
        {
            try
            {
                DatabaseConnection = new SQLiteConnection(String.Format(ConnectionString, "history.dat"));
                DatabaseConnection.Open();

                SQLiteCommand createTblCom = GetCommand(String.Format("CREATE TABLE IF NOT EXISTS {0} {1}", TABLE_NAME, TABLE_DEFINITION));
                createTblCom.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }

        private static SQLiteCommand GetCommand(string sql)
        {
            SQLiteCommand command = DatabaseConnection.CreateCommand();
            command.CommandText = sql;
            return command;
        }

        public static bool Contains(string item)
        {
            SQLiteCommand command = GetCommand(String.Format("SELECT COUNT(*) FROM {0} WHERE {0}.Link='{1}'", TABLE_NAME, item));
            SQLiteDataReader reader = command.ExecuteReader(System.Data.CommandBehavior.SingleResult);

            if (reader.HasRows && reader.Read())
                return reader.GetInt64(0) > 0;
            else
                return false;
        }

        public static bool Add(string link, string title)
        {
            SQLiteCommand command = GetCommand(
                String.Format("INSERT INTO {0} (Link, Title, DownloadedOn) VALUES (@link, @title, @time)", TABLE_NAME));
            command.Parameters.Add(new SQLiteParameter("@link", link));
            command.Parameters.Add(new SQLiteParameter("@title", title));
            command.Parameters.Add(new SQLiteParameter("@time", DateTime.Now.Ticks.ToString()));
            int retCode = command.ExecuteNonQuery();
            return retCode >= 1;
        }

        public static long Count()
        {
            SQLiteCommand command = GetCommand(String.Format("SELECT COUNT (*) FROM {0}", TABLE_NAME));
            SQLiteDataReader reader = command.ExecuteReader();

            if (reader.HasRows && reader.Read())
                return reader.GetInt64(0);
            else
                return -1;
        }
    }
}
