﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfAnimatedGif;
using System.IO;

namespace ImageViewer
{
    /// <summary>
    /// Interaction logic for ImageViewer.xaml
    /// </summary>
    public partial class Viewer : Window
    {
        private Thread DisplayThread { get; set; }
        public event OnBeforeImageShownHandler OnBeforeImageShown;
        public delegate void OnBeforeImageShownHandler(string path);
        public event OnAfterImageShownHandler OnAfterImageShown;
        public delegate void OnAfterImageShownHandler(string path);
        public event OnImageQueuedHandler OnImageQueued;
        public delegate void OnImageQueuedHandler(string path);
        public event OnImageDequeuedHandler OnImageDequeued;
        public delegate void OnImageDequeuedHandler(string path);
        public Duration MinDisplayTime { get; set; }
        private bool Run { get; set; }
        private string RootPath { get; set; }

        private Viewer()
        {
            InitializeComponent();
            Run = true;
            MinDisplayTime = new Duration(new TimeSpan(0));
            DisplayThread = new Thread(new ThreadStart(DisplayImages));
            DisplayThread.Start();
        }

        public Viewer(string path)
            : this()
        {
            RootPath = path;
        }

        private void DisplayImages()
        {
            while (Run)
            {
                if (String.IsNullOrWhiteSpace(RootPath))
                    return;

                foreach (string path in Directory.EnumerateFiles(RootPath, "*", SearchOption.AllDirectories))
                {
                    if (!Run)
                        return;

                    if (OnImageDequeued != null)
                        OnImageDequeued.Invoke(path);

                    Duration gifDuration = new Duration();

                    if (OnBeforeImageShown != null)
                        OnBeforeImageShown.Invoke(path);

                    TimeSpan fadeOut = TimeSpan.FromSeconds(0.1);
                    TimeSpan fadeIn = TimeSpan.FromSeconds(0.1);

                    this.Dispatcher.InvokeAsync(() =>
                    {
                        try
                        {
                            var image = new BitmapImage(new Uri(path));
                            gifDuration = GetGifDuration(path) + new Duration(TimeSpan.FromSeconds(1));
                            SourceImage.ChangeSource(image, fadeOut, fadeIn);
                            Title = path;
                        }
                        catch (Exception) { }
                    });

                    Thread.Sleep(fadeOut + fadeIn);

                    if (gifDuration > MinDisplayTime)
                    {
                        Thread.Sleep((int) gifDuration.TimeSpan.TotalMilliseconds);
                    }
                    else
                    {
                        Thread.Sleep((int) MinDisplayTime.TimeSpan.TotalMilliseconds);
                    }

                    if (OnAfterImageShown != null)
                        OnAfterImageShown.Invoke(path);
                }
            }
        }

        /// <summary>
        /// Get the total duration of a GIF image
        /// <remarks>
        /// Taken from <para/>
        /// http://stackoverflow.com/questions/17722787/c-sharp-drawing-imaging-dropping-gif-frames-if-they-are-identical
        /// </remarks>
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <returns>Total duration of GIF file</returns>
        private Duration GetGifDuration(string path)
        {
            //If path is not found, we should throw an IO exception
            if (!System.IO.File.Exists(path))
                throw new System.IO.IOException("File does not exist");

            if (!path.EndsWith(".gif", StringComparison.InvariantCultureIgnoreCase))
                return new Duration(new TimeSpan(0, 0, 0, 0, 0));

            try
            {
                //Load the image
                var img = System.Drawing.Image.FromFile(path);

                //Count the frames
                var frameCount = img.GetFrameCount(FrameDimension.Time);

                //If the image is not an animated gif, we should throw an
                //argument exception
                if (frameCount <= 1)
                    return new Duration(new TimeSpan(0, 0, 0, 0, 0));

                //Get the times stored in the gif
                //Time delay, in hundredths of a second, between two frames in an animated GIF image.
                //PropertyTagFrameDelay ((PROPID) 0x5100) comes from gdiplusimaging.h
                //More info on http://msdn.microsoft.com/en-us/library/windows/desktop/ms534416(v=vs.85).aspx
                var times = img.GetPropertyItem(0x5100).Value;

                int duration = 0;
                //Convert the 4bit duration chunk into an int
                for (int i = 0; i < frameCount; i++)
                {
                    //convert 4 bit value to integer
                    duration += BitConverter.ToInt32(times, 4 * i);
                }

                return new Duration(TimeSpan.FromSeconds(duration / 100.0));
            }
            catch (Exception)
            {
                return new Duration(TimeSpan.FromSeconds(0));
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Run = false;
        }
    }

    static class Extension
    {
        public static void ChangeSource(this System.Windows.Controls.Image image, ImageSource source, TimeSpan fadeOutTime, TimeSpan fadeInTime)
        {
            var fadeInAnimation = new DoubleAnimation(1d, fadeInTime);

            if (image.Source != null)
            {
                var fadeOutAnimation = new DoubleAnimation(0d, fadeOutTime);

                fadeOutAnimation.Completed += (o, e) =>
                {
                    ImageBehavior.SetAnimatedSource(image, source);
                    image.Source = source;
                    image.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadeInAnimation);
                };

                image.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadeOutAnimation);
            }
            else
            {
                image.Opacity = 0d;
                image.Source = source;
                image.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadeInAnimation);
            }
        }
    }
}
