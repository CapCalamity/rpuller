﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer
{
    class Controller : INotifyPropertyChanged
    {
        private Viewer ImageViewer { get; set; }

        private string _RootPath;
        public string RootPath
        {
            get { return _RootPath; }
            set { SetField(ref _RootPath, value); }
        }

        public Controller()
        {

        }

        public void StartImageViewer()
        {
            if (ImageViewer == null)
            {
                ImageViewer = new Viewer(RootPath);
                ImageViewer.ShowDialog();
                ImageViewer = null;
            }
        }

        #region Boilerplate code to make INotifyPropertyChanged work
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion
    }
}
